package ru.insta.instataskapp

import android.app.Application
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level
import ru.insta.instataskapp.di.*

class ApplicationWrapper : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin()
    }

    private fun startKoin() {
        startKoin {
            androidLogger(Level.DEBUG)
            androidContext(this@ApplicationWrapper)
            modules(
                listOf(
                    viewModelsModule,
                    repositoriesModule,
                    networkModule,
                    servicesModule,
                    preferenceModule
                )
            )
        }
    }
}