package ru.insta.instataskapp.webservice

import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST
import ru.insta.instataskapp.Network
import ru.insta.instataskapp.auth.model.LoginRequestModel

interface LoginApiService {

    @Headers("Content-Type: application/json")
    @POST(Network.AUTH_END_POINT)
    suspend fun login(@Body loginRequestModel: LoginRequestModel): Response<String?>?
}