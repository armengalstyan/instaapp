package ru.insta.instataskapp.webservice

import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.QueryMap
import ru.insta.instataskapp.Network
import ru.insta.instataskapp.main.model.response.SignalsResponseModel

interface SignalsApiService {

    @GET(Network.SIGNALS_END_POINT)
    suspend fun getSignals(
        @QueryMap map: MutableMap<String, Any>
    ): Response<List<SignalsResponseModel>>?
}