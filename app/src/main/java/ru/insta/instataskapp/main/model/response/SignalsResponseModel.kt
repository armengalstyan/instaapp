package ru.insta.instataskapp.main.model.response

import com.google.gson.annotations.SerializedName

data class SignalsResponseModel(

	@field:SerializedName("Comment")
	val comment: String? = null,

	@field:SerializedName("Price")
	val price: Double? = null,

	@field:SerializedName("ActualTime")
	val actualTime: Int? = null,

	@field:SerializedName("TradingSystem")
	val tradingSystem: Int? = null,

	@field:SerializedName("Period")
	val period: String? = null,

	@field:SerializedName("Sl")
	val sl: Double? = null,

	@field:SerializedName("Id")
	val id: Int? = null,

	@field:SerializedName("Cmd")
	val cmd: Int? = null,

	@field:SerializedName("Tp")
	val tp: Double? = null,

	@field:SerializedName("Pair")
	val pair: String? = null
)