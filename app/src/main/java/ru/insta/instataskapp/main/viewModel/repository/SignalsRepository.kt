package ru.insta.instataskapp.main.viewModel.repository

import retrofit2.Response
import ru.insta.instataskapp.helper.Helper
import ru.insta.instataskapp.main.model.request.SignalsRequestModel
import ru.insta.instataskapp.main.model.response.SignalsResponseModel
import ru.insta.instataskapp.webservice.SignalsApiService

private interface SignalsRepository {

    suspend fun getSignals(signalsRequestModel: SignalsRequestModel): Response<List<SignalsResponseModel>>?
}

class SignalsRepositoryImpl(private val signalsApiService: SignalsApiService) : SignalsRepository {

    override suspend fun getSignals(signalsRequestModel: SignalsRequestModel): Response<List<SignalsResponseModel>>? {
        return signalsApiService.getSignals(Helper.toQueryMap(signalsRequestModel))
    }
}