package ru.insta.instataskapp.main.model.request

import com.google.gson.annotations.SerializedName

data class SignalsRequestModel(
    @SerializedName("tradingsystem") val tradingSystem: Int,
    @SerializedName("pairs") val pairs: String,
    @SerializedName("from") val from: Long,
    @SerializedName("to") val to: Long
)

