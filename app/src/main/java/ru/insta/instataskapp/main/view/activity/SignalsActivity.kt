package ru.insta.instataskapp.main.view.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import org.koin.androidx.viewmodel.ext.android.viewModel
import ru.insta.instataskapp.common.Resource.Loading
import ru.insta.instataskapp.common.Resource.Success
import ru.insta.instataskapp.common.Resource.Error
import ru.insta.instataskapp.databinding.ActivitySignalsBinding
import ru.insta.instataskapp.helper.hide
import ru.insta.instataskapp.helper.show
import ru.insta.instataskapp.main.view.adapter.SignalsRecyclerAdapter
import ru.insta.instataskapp.main.viewModel.SignalsViewModel

class SignalsActivity : AppCompatActivity() {

    private lateinit var mBinding: ActivitySignalsBinding

    private val mSignalsViewModel by viewModel<SignalsViewModel>()

    private val mSignalsRecyclerAdapter by lazy {
        val signalsRecyclerAdapter = SignalsRecyclerAdapter(mutableListOf())
        mBinding.signalsRecyclerView.apply {
            layoutManager = LinearLayoutManager(this@SignalsActivity)
            adapter = signalsRecyclerAdapter
            hasFixedSize()
        }
        return@lazy signalsRecyclerAdapter
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initBinding()
        setContentView(mBinding.root)

        mSignalsViewModel.getSignals()
        initObservers()
    }

    private fun initBinding() {
        mBinding = ActivitySignalsBinding.inflate(layoutInflater)
    }

    private fun initObservers() {
        mSignalsViewModel.getSignalsLiveData().observe(this, Observer { response ->
            when (response) {
                is Loading -> mBinding.progressBar.show()
                is Success -> {
                    mBinding.progressBar.hide()
                    val signalItems = response.data
                    if (signalItems != null) {
                        mSignalsRecyclerAdapter.apply {
                            setSignalItems(signalItems)
                            notifyDataSetChanged()
                        }
                    }
                }
                is Error -> {
                    mBinding.progressBar.hide()
                    Toast.makeText(this, response.message, Toast.LENGTH_LONG).show()
                }
            }
        })
    }
}
