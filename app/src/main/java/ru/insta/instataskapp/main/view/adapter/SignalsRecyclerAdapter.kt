package ru.insta.instataskapp.main.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import ru.insta.instataskapp.R
import ru.insta.instataskapp.databinding.SignalRecyclerViewItemBinding
import ru.insta.instataskapp.main.model.response.SignalsResponseModel

class SignalsRecyclerAdapter(private var signalList: List<SignalsResponseModel>?) :
    RecyclerView.Adapter<SignalsRecyclerAdapter.SignalsRecyclerViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SignalsRecyclerViewHolder {
        val binding: SignalRecyclerViewItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.signal_recycler_view_item,
            parent,
            false
        )
        return SignalsRecyclerViewHolder(binding)
    }

    override fun onBindViewHolder(holder: SignalsRecyclerViewHolder, position: Int) {
        holder.bind(signalList?.get(position))
    }

    override fun getItemCount(): Int {
        return signalList?.size ?: 0
    }

    fun setSignalItems(signalList: List<SignalsResponseModel>?) {
        this.signalList = signalList
    }

    class SignalsRecyclerViewHolder(private val binding: SignalRecyclerViewItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(signalItem: SignalsResponseModel?) {
            binding.signalItem = signalItem
        }
    }
}