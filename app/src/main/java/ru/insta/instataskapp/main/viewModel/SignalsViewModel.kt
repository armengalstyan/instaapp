package ru.insta.instataskapp.main.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import ru.insta.instataskapp.common.Resource
import ru.insta.instataskapp.main.model.request.SignalsRequestModel
import ru.insta.instataskapp.main.model.response.SignalsResponseModel
import ru.insta.instataskapp.main.viewModel.repository.SignalsRepositoryImpl

class SignalsViewModel(private val signalsRepositoryImpl: SignalsRepositoryImpl) : ViewModel() {

    private val mSignalsMutableLiveData: MutableLiveData<Resource<List<SignalsResponseModel>>> by lazy {
        return@lazy MutableLiveData<Resource<List<SignalsResponseModel>>>()
    }

    fun getSignals() = viewModelScope.launch {
        mSignalsMutableLiveData.value = Resource.Loading()
        val response = signalsRepositoryImpl.getSignals(
            SignalsRequestModel(
                3, "EURUSD, GBPUSD, USDJPY, USDCHF, USDCAD, AUDUSD, NZDUSD",
                1479860023, 1480066860
            )
        )
        if (response != null) {
            if (response.isSuccessful && response.body() != null) {
                mSignalsMutableLiveData.value = Resource.Success(response.body()!!)
            } else {
                mSignalsMutableLiveData.value = Resource.Error(response.errorBody().toString())
            }
        } else {
            mSignalsMutableLiveData.value = Resource.Error("Response is null")
        }
    }

    fun getSignalsLiveData(): LiveData<Resource<List<SignalsResponseModel>>> =
        mSignalsMutableLiveData
}