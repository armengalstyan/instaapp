package ru.insta.instataskapp.di

import com.google.gson.FieldNamingPolicy
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.module.Module
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import ru.insta.instataskapp.BuildConfig
import ru.insta.instataskapp.Network
import ru.insta.instataskapp.Preference
import ru.insta.instataskapp.auth.viewmodel.LoginViewModel
import ru.insta.instataskapp.auth.viewmodel.repository.LoginRepositoryImpl
import ru.insta.instataskapp.helper.SharedPrefs
import ru.insta.instataskapp.main.viewModel.SignalsViewModel
import ru.insta.instataskapp.main.viewModel.repository.SignalsRepositoryImpl
import ru.insta.instataskapp.webservice.LoginApiService
import ru.insta.instataskapp.webservice.SignalsApiService
import java.util.concurrent.TimeUnit

val viewModelsModule: Module = module {

    viewModel { LoginViewModel(get()) }

    viewModel { SignalsViewModel(get()) }
}

val repositoriesModule: Module = module {

    single { LoginRepositoryImpl(get()) }

    single { SignalsRepositoryImpl(get()) }
}

val networkModule: Module = module {
    // Gson
    single {
        GsonBuilder()
            .setFieldNamingPolicy(FieldNamingPolicy.IDENTITY)
            .setPrettyPrinting()
            .create()
    }

    // OkHttpClient
    single {
        val okHttpBuilder = OkHttpClient.Builder()
        val logging = HttpLoggingInterceptor()

        if (BuildConfig.DEBUG) {
            logging.level = HttpLoggingInterceptor.Level.BODY
        } else {
            logging.level = HttpLoggingInterceptor.Level.BASIC
        }

        okHttpBuilder
            .addInterceptor(logging)
            .connectTimeout(30, TimeUnit.SECONDS)
            .writeTimeout(10, TimeUnit.MINUTES) // fix exc. when uploading files with slow internet
            .readTimeout(1, TimeUnit.MINUTES)
            .addInterceptor(object : Interceptor {
                override fun intercept(chain: Interceptor.Chain): Response {

                    val originalRequest = chain.request()

                    val token = get<SharedPrefs>().getValue(Preference.TOKEN_KEY)
                    if (token != null) {
                        val newRequest = chain.request().newBuilder()
                            .addHeader(
                                "passkey", token
                            )
                            .method(originalRequest.method, originalRequest.body)
                            .build()

                        val response = chain.proceed(newRequest)
                        if (response.code == 401) {
                            // Handle 401
                        }
                        return response
                    }
                    return chain.proceed(originalRequest)
                }
            })
            .build()
    }

    // Retrofit
    single {
        Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create(get<Gson>()))
            .baseUrl(Network.BASE_URL)
            .client(get<OkHttpClient>())
            .build()
    }
}

val servicesModule: Module = module {
    single { get<Retrofit>().create(LoginApiService::class.java) }
    single { get<Retrofit>().create(SignalsApiService::class.java) }
}

val preferenceModule: Module = module {
    single { SharedPrefs(androidContext()) }
}