package ru.insta.instataskapp.helper

import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences
import ru.insta.instataskapp.Preference

class SharedPrefs(context: Context) {

    private var mSharedPreferences: SharedPreferences =
        context.getSharedPreferences(Preference.PREFS_FILE_NAME, MODE_PRIVATE)

    fun putValue(key: String, value: String) {
        mSharedPreferences.edit().putString(key, value).apply()
    }

    fun getValue(key: String): String? = mSharedPreferences.getString(key, null)
}