package ru.insta.instataskapp.helper

import ru.insta.instataskapp.main.model.request.SignalsRequestModel

class Helper {
    companion object {
        fun toQueryMap(signalsRequestModel: SignalsRequestModel) : MutableMap<String, Any> {
            val queryMap: MutableMap<String, Any> = HashMap()
            queryMap["tradingsystem"] = signalsRequestModel.tradingSystem
            queryMap["pairs"] = signalsRequestModel.pairs
            queryMap["from"] = signalsRequestModel.from
            queryMap["to"] = signalsRequestModel.to
            return queryMap
        }
    }
}