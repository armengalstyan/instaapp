package ru.insta.instataskapp

object Network {

    const val BASE_URL = "http://client-api.instaforex.com/"

    const val AUTH_END_POINT = "api/Authentication/RequestMoblieCabinetApiToken"

    const val SIGNALS_END_POINT = "clientmobile/GetAnalyticSignals/20234561"
}

object Preference {

    const val PREFS_FILE_NAME = "preference"

    const val TOKEN_KEY = "token"
}

object App {

    const val SPLASH_DELAY_TIME = 2000L
}