package ru.insta.instataskapp.splash.view

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject
import ru.insta.instataskapp.App
import ru.insta.instataskapp.Preference
import ru.insta.instataskapp.R
import ru.insta.instataskapp.helper.SharedPrefs
import ru.insta.instataskapp.auth.view.LoginActivity
import ru.insta.instataskapp.main.view.activity.SignalsActivity

class SplashActivity : AppCompatActivity() {

    private val sharedPrefs by inject<SharedPrefs>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        GlobalScope.launch {
            delay(App.SPLASH_DELAY_TIME)
            checkLogin()
        }
    }

    private fun checkLogin() {
        val token: String? = sharedPrefs.getValue(Preference.TOKEN_KEY)
        val intent = if (token != null) {
            Intent(this@SplashActivity, SignalsActivity::class.java)
        } else {
            Intent(this@SplashActivity, LoginActivity::class.java)
        }
        startActivity(intent)
        finish()
    }
}
