package ru.insta.instataskapp.auth.model

import com.google.gson.annotations.SerializedName

data class LoginRequestModel(
    @SerializedName("Login") val login: String,
    @SerializedName("Password") val password: String
)