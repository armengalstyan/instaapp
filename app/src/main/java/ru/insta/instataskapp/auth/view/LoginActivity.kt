package ru.insta.instataskapp.auth.view

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel
import ru.insta.instataskapp.Preference
import ru.insta.instataskapp.helper.SharedPrefs
import ru.insta.instataskapp.auth.model.LoginRequestModel
import ru.insta.instataskapp.auth.viewmodel.LoginViewModel
import ru.insta.instataskapp.common.Resource.Loading
import ru.insta.instataskapp.common.Resource.Success
import ru.insta.instataskapp.databinding.ActivityLoginBinding
import ru.insta.instataskapp.helper.hide
import ru.insta.instataskapp.helper.show
import ru.insta.instataskapp.main.view.activity.SignalsActivity

class LoginActivity : AppCompatActivity() {

    private lateinit var mBinding: ActivityLoginBinding

    private val mLoginViewModel by viewModel<LoginViewModel>()
    private val mSharedPrefs by inject<SharedPrefs>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initBinding()
        setContentView(mBinding.root)
        setOnClickListeners()
        initObservers()
    }

    private fun initBinding() {
        mBinding = ActivityLoginBinding.inflate(layoutInflater)
    }

    private fun setOnClickListeners() {
        mBinding.loginButton.setOnClickListener {
            mLoginViewModel.login(LoginRequestModel(login = "20234561", password = "ladevi31"))
        }
    }

    private fun initObservers() {
        mLoginViewModel.getLoginLiveData().observe(this, Observer { response ->
            when (response) {
                is Loading -> mBinding.progressBar.show()
                is Success -> {
                    mBinding.progressBar.hide()
                    val token = response.data
                    if (token != null) {
                        mSharedPrefs.putValue(Preference.TOKEN_KEY, token)
                        Intent(this, SignalsActivity::class.java).also { intent ->
                            startActivity(
                                intent
                            )
                        }
                        finish()
                    }
                }
                is Error -> {
                    mBinding.progressBar.hide()
                    Toast.makeText(this, response.message, Toast.LENGTH_LONG).show()
                }
            }
        })
    }
}
