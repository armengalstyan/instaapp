package ru.insta.instataskapp.auth.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import ru.insta.instataskapp.auth.model.LoginRequestModel
import ru.insta.instataskapp.auth.viewmodel.repository.LoginRepositoryImpl
import ru.insta.instataskapp.common.Resource

class LoginViewModel(private val loginRepositoryImpl: LoginRepositoryImpl) : ViewModel() {

    private val mLoginMutableLiveData: MutableLiveData<Resource<String>> by lazy {
        return@lazy MutableLiveData<Resource<String>>()
    }

    fun login(loginRequestModel: LoginRequestModel) = viewModelScope.launch {
        mLoginMutableLiveData.value = Resource.Loading()
        val response = loginRepositoryImpl.login(loginRequestModel)
        if (response != null) {
            if (response.isSuccessful && response.body() != null) {
                mLoginMutableLiveData.value = Resource.Success(response.body()!!)
            } else {
                mLoginMutableLiveData.value = Resource.Error(response.errorBody().toString())
            }
        } else {
            mLoginMutableLiveData.value = Resource.Error("Response is null")
        }
    }

    fun getLoginLiveData(): LiveData<Resource<String>> = mLoginMutableLiveData
}