package ru.insta.instataskapp.auth.viewmodel.repository

import retrofit2.Response
import ru.insta.instataskapp.auth.model.LoginRequestModel
import ru.insta.instataskapp.webservice.LoginApiService

private interface LoginRepository {

    suspend fun login(loginRequestModel: LoginRequestModel): Response<String?>?
}

class LoginRepositoryImpl(private val apiService: LoginApiService) : LoginRepository {

    override suspend fun login(loginRequestModel: LoginRequestModel): Response<String?>? {
        return apiService.login(loginRequestModel)
    }
}